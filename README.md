# Start_page

My own Start Page (for my browser).

## Features
---

This start page contains a lot of basic features which help you to have an easy acces to all you need ! You just need to change links in the script to customize this page !

- My gitlab (change the document.form.action link to go to your Gitlab) : press <kbd>/</kbd> + <kbd>g</kbd>

- Reddit (r/oddlysatisfying/) : press <kbd>/</kbd> + <kbd>r</kbd>

- Google Classroom : press <kbd>/</kbd> + <kbd>cm</kbd>

- Codeberg : press <kbd>/</kbd> + <kbd>cb</kbd>

- Help : press ```--help```

- Weather : press <kbd>/</kbd> + <kbd>owm</kbd> with your city name :
    - Ex : <kbd>/owm Paris</kbd>


* Press anything to search at DuckduckGo (you can also change it for Google)

## How it works ?
---

It's just a simple HTML form :

![Form](img/form.png)

With a simple JS script :

![JS](img/js.png)

## Updates
---

I just added my boilerplates : press <kbd>/</kbd> + <kbd>b</kbd>

:boom: The RSS reader is working ! Press <kbd>/</kbd> + <kbd>rss</kbd>

---

I will certainly add some cool features, ~~like a RSS feed...~~