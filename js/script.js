var load_div = document.getElementById("load_text")
var text_area = document.getElementById("search")
var reload = document.getElementById("rl_button")

var g = 0;

var day =["Today", "Tomorrow", "A.Tomorrow", "D.A.Tomorrow"]

var city = "";

function HandleSubmission(){
   if (text_area.value === "My Gitlab" || text_area.value === "Gitlab" || text_area.value === "gitlab" || text_area.value === "/g") {
      document.form.action = "https://gitlab.com/a70m0s";
      form.submit();
   } else if (text_area.value === "My Reddit" || text_area.value === "Reddit" || text_area.value === "reddit" || text_area.value === "/r"){
      document.form.action = "https://libredd.it/r/oddlysatisfying/";
      form.submit();
   } else if (text_area.value === "My Codeberg" || text_area.value === "Codeberg" || text_area.value === "codeberg" || text_area.value === "/cb") {
      document.form.action = "https://codeberg.org/";
      form.submit();
   } else if (text_area.value === "My Classroom" || text_area.value === "Classroom" || text_area.value === "classroom" || text_area.value === "/cm"){
      document.form.action = "https://edu.google.com/intl/fr_fr/products/classroom/";
      form.submit();
   } else if (text_area.value === "/b"){
      document.form.action = "pages/blp.html";
      form.submit();
   } else if (text_area.value === "/rss"){
      loadXMLDoc(); 
   } else if (text_area.value === "/gd"){
      document.form.action = "https://drive.google.com";
      form.submit();
   } else if (text_area.value === "/i"){
      document.form.action = "https://a70m0s.gitlab.io/A70m0s";
      form.submit();
   } else if (text_area.value === "--help"){
      helpConsole();
   } else {
      document.form.action = "https://duckduckgo.com";
      form.submit();
      text_area.value='';
   }
}

function CheckForEnterClick(e){
   e = e || window.event;
   if( e.keyCode === 13 || e.which === 13 ) { checkWeather(); }
}

var addEventHandler = function(element,eventType,functionRef){
    if( element === null || typeof(element) === 'undefined' ) { return; }
    if( element.addEventListener ) { element.addEventListener(eventType,functionRef,false); }
    else if( element.attachEvent ) { element.attachEvent("on"+eventType,functionRef); }
    else { element["on"+eventType] = functionRef; }
};

addEventHandler(window,"keydown",CheckForEnterClick);

function loadXMLDoc() {
   var xmlhttp = new XMLHttpRequest();
   xmlhttp.onreadystatechange = function() {
   myFunction(this);
   };
   xmlhttp.open("GET", "https://www.ouest-france.fr/rss-en-continu.xml", true);
   xmlhttp.send();
   setTimeout(loadXMLDoc, 10000);
}

function myFunction(xml) {
   var xmlDoc, txt;
   xmlDoc = xml.responseXML;
   txt = "";
   title = xmlDoc.getElementsByTagName("title");
   informations = xmlDoc.getElementsByTagName("description");
   link = xmlDoc.getElementsByTagName("link");
   txt += "<h5>" + title[2].childNodes[0].nodeValue + "</h5>" + "<br>" + "<p>" + informations[1].childNodes[0].nodeValue + "</p>" + "<a href='" + link[2].childNodes[0].nodeValue + "' target='_blank' rel='external noopener noreferrer'>Lien</a>";
   document.getElementById("rss_txt").innerHTML = txt;
}

function checkForm() {
   return false;
}

   var callBackGetSuccess = function(data) {
      for (; g<5; g++) {
         var element = document.getElementById("m_p"+ String(g+1));
         element.innerHTML = "";
         element.innerHTML = "======================<br>";
         element.innerHTML += "<h1>|"+day[g]+"</h1>";
         element.innerHTML += "|Location : " + city;
         element.innerHTML += "<br>|Temp : " + data.list[g].main.temp + "°C";
         element.innerHTML += "<br>|Wind speed : " + data.list[g].wind.speed + "km/h";
         element.innerHTML += "<br>|Sky : " + data.list[g].weather[0].description;
         element.innerHTML += "<br>======================";
         var img = document.getElementById("i"+ String(g))
         if (data.list[g].weather[0].icon === "09d" || data.list[g].weather[0].icon === "09n"){
            img.setAttribute("src", "img/drizzle.svg");
         } else if (data.list[g].weather[0].icon === "11d" || data.list[g].weather[0].icon === "11n"){
            img.setAttribute("src", "img/thunderstorm.svg");
         } else if (data.list[g].weather[0].icon === "10d" || data.list[g].weather[0].icon === "10n"){
            img.setAttribute("src", "img/rain.svg");
         } else if (data.list[g].weather[0].icon === "03d" || data.list[g].weather[0].icon === "04d" || data.list[g].weather[0].icon === "03n" || data.list[g].weather[0].icon === "04n"){
            img.setAttribute("src", "img/cloud.svg");
         }
      }
}


function buttonClickGET(ville) {

   var url = "https://api.openweathermap.org/data/2.5/forecast?q=" + ville + "&units=metric&appid=b385c337efa82d72a89c22128a7e7f80";

   $.get(url, callBackGetSuccess).done(function() {
      })
      .fail(function() {
        alert( "error" );
      })
      .always(function() {
      });
}

function helpConsole() {
   document.getElementById("info").style.display = "none";
   var help = document.getElementById("help");
   help.style.display = "block";
}

function checkWeather() {
   var toto = text_area.value.split(" ");
   if (toto[0] === "/owm") {
      var city = toto[1];
      buttonClickGET(city);
   } else {
      HandleSubmission();
   };
};

function closeTab (id_clicked) {
   if (id_clicked === "red_form") {
      var tab = document.getElementById("formulaire");
      tab.style.display = "none";
   } else if (id_clicked === "red_social") {
      var tab = document.getElementById("social_");
      tab.style.display = "none";
   } else if (id_clicked === "red_rss") {
      var tab = document.getElementById("rss");
      tab.style.display = "none";
   }
};